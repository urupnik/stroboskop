# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://urbic@bitbucket.org/urbic/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/urbic/stroboskop/commits/3ba06dbc8b2194be4260bdc741865a598a6a091a

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/urbic/stroboskop/commits/ed02b0463aa26e72605dafdbfa47235bdbac12e5

Naloga 6.3.2:
https://bitbucket.org/urbic/stroboskop/commits/7cb02d3c5fc43fc2b0be1d576d6ba19c2bcabea2

Naloga 6.3.3:
https://bitbucket.org/urbic/stroboskop/commits/36433a3017ebe929ea31c1552ea3f5fa3ba5f7e4

Naloga 6.3.4:
https://bitbucket.org/urbic/stroboskop/commits/12309901b90012fa61dfdd956a4095b081a5d5f8

Naloga 6.3.5:

```
git checkout master

git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/urbic/stroboskop/commits/51aa88772432f90dd687bd37abe64aaa139adb2b?at=dinamika

Naloga 6.4.2:
https://bitbucket.org/urbic/stroboskop/commits/7acbc0b412e5f35750779179d0b1993c4c9c4396

Naloga 6.4.3:
https://bitbucket.org/urbic/stroboskop/commits/7118a79e3165ae8e621b8e8daae0db099b4a8c8b

Naloga 6.4.4:
https://bitbucket.org/urbic/stroboskop/commits/e0aada5d1b2f8304e89b823f56b5fafcd303ae20